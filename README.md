# Hvad er dette?
Danske termer for alt inden for datalogiens verden.

# Termer:

| Engelsk  | Dansk        |
| -------  | -----        |
| computer | datamat      |
| laptop   | mappedatamat |
| software | programmel   |


## Systemer og sprog

| Engelsk                   | Dansk                               |
| -------                   | -----                               |
| kernel                    | kerne                               |
| shell                     | skald                               |
| trap                      | fælde                               |
| interrupt                 | forstyrrelse/afbrydelse             |
| exception                 | undtagelse                          |
| thread                    | tråd                                |
| source code               | kildekode                           |
| interpreter               | fortolker                           |
| compiler                  | oversætter                          |
| transpiler                | tværsoversætter                     |
| linker                    | sammenkæder                         |
| namespace                 | navnerum                            |
| identifier                | identifikator                       |
| loop                      | løkke                               |
| expression                | udtryk                              |
| statement                 | erklæring (nogle gange også udtryk) |
| if (statement/expression) | hvis(udtryk/erklæring)              |
| while loop                | (i)mensløkke                        |
| for loop                  | forløkke                            |
| unroll (kontekst, løkker) | afløkke                             |
| garbage collector         | spild opsamler                      |
| Pointer                   | peger                               |


## Objekt-orienteret

| Engelsk     | Dansk                                 |
| -------     | -----                                 |
| object      | objekt                                |
| class       | klasse                                |
| inheritence | nedarvning                            |
| interface   | grænseflade                           |
| superclass  | forfædre/forældreklasse/overklasse    |
| subclass    | arvningklasse/børneklasse/underklasse |
| instance    | instans/forekomst                     |
| method      | metode                                |
| attribute   | attribut                              |
| property    | egenskab                              |
| field       | felt                                  |


## Hårdevarer

| Engelsk    | Dansk                    |
| -------    | -----                    |
| CPU        | (Central)beregningsenhed |
| GPU        | grafisk beregningsenheds |
| core       | kerne                    |
| memory/RAM | hukommelse               |
| cache      | mellemlager              |
| storage    | lager                    |
| SSD        | solidtilstandslager      |
| HDD        | fastpladelager           |


## Git

| Engelsk       | Dansk                    |
| -------       | -----                    |
| git           | nar (sjælden brug)       |
| pull          | træk/hiv/hal             |
| push          | skub                     |
| commit        | forpligt/forpligtelse    |
| branch        | gren/afgren              |
| master        | mester                   |
| merge         | flet/sammenflæt          |
| cherry-pick   | håndplukke               |
| blame         | bebrejde/beskyld         |
| pull-request  | trækkeanmodning          |
| merge-request | sammenfletningsanmodning |
| stash         | gem                      |
| issue         | problem                  |
| checkout      | tjek ud                  |
| repository    | depot                    |


## Teknologier

| Engelsk     | Dansk       |
| -------     | -----       |
| blockchain  | e-guirlande |
| bitcoin     | e-mønt      |
| game engine | spilmotor   |
| framework   | rammeværk   |
| makefile    | skaberfil   |

## Internet

| Engelsk     | Dansk                         |
| -------     | -----                         |
| WWW         | verdensomspændende spindel    |
| URL         | ensartet resurselokator       |
| DNS         | domænenavnsservice            |
| TCP         | overførselskontrolsprotokol   |
| E-mail      | e-brev                        |
| downloading | nedhenting                    |
| loading     | indlæse                       |
| firewall    | brandvæg/ildmur/cybervoldgrav |
| phishing    | phiske/feske                  |


## Telefon

| Engelsk    | Dansk        |
| -------    | -----        |
| smartphone | lommedatamat |
| android    | androide     |

